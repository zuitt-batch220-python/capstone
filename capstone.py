# Specifications
# 1. Create a Person class that is an abstract class that has the following methods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method

from abc import ABC, abstractmethod


class Person(ABC):
    @abstractmethod
    def get_full_name(self):
        pass

    @abstractmethod
    def add_request(self):
        pass

    @abstractmethod
    def check_request(self):
        pass

    @abstractmethod
    def add_user(self):
        pass


# 2. Create an Employee class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# Inheriting
class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    # Methods
    def get_full_name(self):
        return f"{self.get_first_name()} {self.get_last_name()}"

    def add_request(self):
        return "Request has been added"

    def check_request(self):
        return None

    def add_user(self):
        return None

    def login(self):
        return f"{self.get_email()} has logged in"

    def logout(self):
        return f"{self.get_email()} has logged out"

    # Getter-Setter

    def get_first_name(self):
        return self._first_name

    def set_first_name(self, first_name):
        self._first_name = first_name

    def get_last_name(self):
        return self._last_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def get_email(self):
        return self._email

    def set_email(self, email):
        self._email = email

    def get_department(self):
        return self._department

    def set_department(self, department):
        self._department = department


# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members
#   b. Methods
#       Abstract methods
#       For the addRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addMember() - adds an employee to the members list
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# Inheriting
class Team_Lead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._members = []

    # Methods
    def get_full_name(self):
        return f"{self.get_first_name()} {self.get_last_name()}"

    def add_request(self):
        return None

    def check_request(self):
        return None

    def add_user(self):
        return None

    def login(self):
        return f"{self.get_email()} has logged in"

    def logout(self):
        return f"{self.get_email()} has logged out"

    def add_member(self, member_details):
        return self._members.append(member_details)

    # Getter-Setter
    def get_first_name(self):
        return self._first_name

    def set_first_name(self, first_name):
        self._first_name = first_name

    def get_last_name(self):
        return self._last_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def get_email(self):
        return self._email

    def set_email(self, email):
        self._email = email

    def get_department(self):
        return self._department

    def set_department(self, department):
        self._department = department

    def get_members(self):
        return self._members

    def set_members(self, members):
        self._members = members


# 4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addRequest methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addUser() - outputs "New user added"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__()
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department

    # Methods
    def get_full_name(self):
        return f"{self.get_first_name()} {self.get_last_name()}"

    def add_request(self):
        return None

    def check_request(self):
        return None

    def add_user(self):
        return "User has been added"

    def login(self):
        return f"{self.get_email()} has logged in"

    def logout(self):
        return f"{self.get_email()} has logged out"

    # Getter-Setter
    def get_first_name(self):
        return self._first_name

    def set_first_name(self, first_name):
        self._first_name = first_name

    def get_last_name(self):
        return self._last_name

    def set_last_name(self, last_name):
        self._last_name = last_name

    def get_email(self):
        return self._email

    def set_email(self, email):
        self._email = email

    def get_department(self):
        return self._department

    def set_department(self, department):
        self._department = department


# 5. Create a Request that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods
#       updateRequest
#       closeRequest
#       cancelRequest
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled

class Request():
    def __init__(self, name, requester, date_requested):
        self.name = name
        self.requester = requester
        self.date_requested = date_requested
        self.status = "open"

    # Methods
    def update_request(self):
        return f"Request {self.get_name()} has been updated"

    def close_request(self):
        return f"Request {self.get_name()} has been closed"

    def cancel_request(self):
        return f"Request {self.get_name()} has been cancelled"

    # Getter-Setter
    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_requester(self):
        return self.requester

    def set_requester(self, requester):
        self.requester = requester

    def get_date_requested(self):
        return self.date_requested

    def set_date_requested(self, date_requested):
        self.date_requested = date_requested

    def get_status(self):
        return self._status

    def set_status(self, status):
        self._status = status


# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
# print(employee1.get_full_name())
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
team_lead1 = Team_Lead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", team_lead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.get_full_name() == "John Doe", "Full name should be John Doe"
# print(employee1.get_full_name())
assert admin1.get_full_name() == "Monika Justin", "Full name should be Monika Justin"
# print(admin1.get_full_name())
assert team_lead1.get_full_name() == "Michael Specter", "Full name should be Michael Specter"
# print(team_lead1.get_full_name())
assert employee2.login() == "sjane@mail.com has logged in"
# print(employee2.login())
assert employee2.add_request() == "Request has been added"
# print(employee2.add_request())
assert employee2.logout() == "sjane@mail.com has logged out"
team_lead1.add_member(employee3)
team_lead1.add_member(employee4)
for indiv_emp in team_lead1.get_members():
    print(indiv_emp.get_full_name())

assert admin1.add_user() == "User has been added"

req2.set_status("closed")
print(req2.close_request())
